# Wenhao Chen ID:873510615
# This project is to simulate the percentage that the hand of dealer was bust.
import random
import sys

def get_card():
	return random.randint(1,13)

def score(cards):
	total = 0
	soft_ace_count = 0
	for i in cards:
		# J,Q,K situation
		if i == 11 or i == 12 or i == 13:
			total = total + 10
			# soft ace can avoid bust
			if total > 21 and soft_ace_count == 1:
				total = total - 10
				soft_ace_count = 0
		# Ace situation
		elif i == 1:
			if total + 11 <= 21:
				total = total + 11
				soft_ace_count = 1
			else:
				total = total + 1
			# soft ace can avoid bust
			if total > 21 and soft_ace_count == 1:
				total = total - 10
				soft_ace_count = 0
		# 2-10 situation
		else:
			total = total + i
			# soft ace can avoid bust
			if total > 21 and soft_ace_count == 1:
				total = total - 10
				soft_ace_count = 0
	return (total, soft_ace_count)


def stand(stand_on_value, stand_on_soft, cards): 
	total, soft_ace_count = score(cards)
	if stand_on_soft == True:
		if total < stand_on_value:
			return False
		else:
			return True
	if stand_on_soft == False:
		if total < stand_on_value:
			return False
		elif total == stand_on_value and soft_ace_count == 1:
			return False
		else:
			return True

def main():
	arg = sys.argv
	# perform basic validation on the arguments
	arg[1] = int(arg[1])
	arg[2] = int(arg[2])
	if arg[1]<=0 or arg[2]<1 or arg[2]>20 or arg[3] != 'hard' and arg[3] != 'soft':
		raise ValueError('The arguments are illegal')
	# simulations
	bust = 0
	for i in range(arg[1]):
		cards = [get_card()]
		cards.append(get_card())
		while stand(arg[2], arg[3] == 'soft', cards) == False:
			cards.append(get_card())
		if score(cards)[0] > 21:
			bust += 1
	return bust/arg[1]

if __name__ == '__main__':
	print(main())