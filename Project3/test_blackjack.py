# Wenhao Chen ID:873510615
import blackjack as bj
import unittest
import sys

class TestBlackjack(unittest.TestCase):
	def setUp(self):
		self.percentage = 0

	def test_score(self):
		self.assertEqual(bj.score([3, 12]), (13, 0))
		self.assertEqual(bj.score([5, 5, 10]), (20, 0))
		self.assertEqual(bj.score([11, 10, 1]), (21, 0))
		self.assertEqual(bj.score([1, 1, 5]), (17, 1))
		self.assertEqual(bj.score([1, 1, 1, 7]), (20, 1))
		self.assertEqual(bj.score([7, 8, 10]), (25, 0))
		self.assertEqual(bj.score([1, 6, 8, 10]), (25, 0))

	def test_s12(self):
		sys.argv = ['blackjack.py', '10000', '12', 'hard']
		self.assertEqual(bj.main(), self.percentage)

if __name__ == '__main__':
	unittest.main()