import os
from collections import namedtuple

Record = namedtuple('Record', ['mpg', 'cylinders', 'displacement', 'horsepower', 'weight', 'acceleration', 'model_year', 'origin', 'car_name'])
class AutoMPG():
	def __init__(self, make, model, year, mpg):
		self.make = make
		self.model = model
		self.year = year
		self.mpg = mpg
	
	def __repr__(self):
		return f'{self.make}, {self.model}, {self.year}, {self.mpg}'
	
	def __eq__(self, other):
		if type(self) == type(other):
			return self.make == other.make and self.model == other.model and self.year == other.year and self.mpg == other.mpg
	
	def __lt__(self, other):
		if type(self) == type(other):
			return self.make < other.make and self.model < other.model and self.year < other.year and self.mpg < other.mpg

	def __hash__(self):
		return hash(self.make, self.model, self.year, self.mpg)

class AutoMPGData():
	def __init__(self):
		self.data = self._load_data()
		self.index = 0

	def __iter__(self):
		return self

	def __next__(self):		# iterate the data
		if self.index >= len(self.data)-1:
			raise StopIteration
		else:
			rep = self.index
			self.index += 1
			return self.data[rep]

	def _load_data(self):
		path = r"F:\CourseWork\Pydev\auto-mpg.clean.txt"
		# chack the whether path exists
		if not os.path.exists(path):
			self._clean_data()
		#load data
		data = []
		with open(path) as file:
			lines = file.readlines()
			for line in lines:
				# split all the space and \n
				line = line.rstrip("\n").split()
				line[8] = ' '.join(line[8:]).rstrip('"').lstrip('"')
				line[6] = '19' + line[6]
				# put all the arguments in a namedtuple
				record = Record(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8])
				data.append(record)
		return data

	def _clean_data(self):
		path1 = r"F:\CourseWork\Pydev\auto-mpg.data.txt"
		path2 = r"F:\CourseWork\Pydev\auto-mpg.clean.txt"
		# convert the TAB character to spaces and create the new file
		cleaned = []
		with open(path1) as file:
			lines = file.readlines()
			for line in lines:
				line=line.expandtabs()
				cleaned.append(line)
		with open(path2, 'w') as file:
			for i in cleaned:
				file.writelines(i)
def main():
	for a in AutoMPGData():
		MPG = AutoMPG(a.car_name.split()[0], ' '.join(a.car_name.split()[1:]), int(a.model_year), float(a.mpg))
		print(f'AutoMPG({MPG})')

if __name__ == '__main__':
	print(main())