# Wenhao Chen ID:873510615
# This assignment is to display the auto mpg dataset in a csv file(by using argparse)
import os
import csv
from collections import namedtuple
import logging
import requests
import sys
import argparse
import matplotlib.pyplot as plt

class AutoMPG:
    ''' Class to represent an automobile, containing its make, model, year,
    and miles per gallon
    '''

    def __init__(self, make, model, year, mpg):
        logger.debug(f'creating AutoMPG object: make = {make}, model = {model}, year = {year}, mpg = {mpg}')
        self.make = make
        self.model = model
        self.year = year
        self.mpg = mpg

    def __repr__(self):
        return f"AutoMPG(\'{self.make}\',\'{self.model}\',\'{self.year}\',\'{self.mpg}\')"

    def __str__(self):
        return self.__repr__()

    def __eq__(self,other):
        if type(self) == type(other):
            logger.debug(f'comparing {self.__str__()} == {other.__str__()}: {self.make == other.make and self.model == other.model and self.year == other.year and self.mpg == other.mpg}')
            return self.make == other.make and self.model == other.model and \
                   self.year == other.year and self.mpg == other.mpg
        else:
            logger.info(f'equality not implemented in AugoMPG class for types {type(self)} and {type(other)}')
            return NotImplemented

    def __lt__(self,other):
        ''' Compares the make lexicographically: if they are equal, compares the model;
        if they are equal, compares the year; if they are equal, compares the mpg
        '''

        if type(self) == type(other):
            if self.make != other.make:
                logger.debug(f'makes not equal for {self.__str__()} and {other.__str__()}: {self.make} < {other.make} {self.make < other.make}')
                return self.make < other.make
            if self.model != other.model:
                logger.debug(f'makes equal but models not equal for {self.__str__()} and {other.__str__()}: {self.model} < {other.model} {self.model < other.model}')
                return self.model < other.model
            if self.year != other.year:
                logger.debug(f'makes and models equal but years not equal for {self.__str__()} and {other.__str__()}: {self.year} < {other.year} {self.year < other.year}')
                return self.year < other.year

            logger.debug(f'makes, models, and years equal but mpg not equal for {self.__str__()} and {other.__str__()}: {self.mpg} < {other.mpg} {self.mpg < other.mpg}')
            return self.mpg < other.mpg

        else:
            logger.info(f'< not implemented in AugoMPG class for types {type(self)} and {type(other)}')
            return NotImplemented

    def __hash__(self):
        ## Since all instance variables are immutable types, put them in a tuple
        ## and hash the tuple
        logger.debug(f'hash value for {self.__str__()}: {hash((self.make,self.model,self.year,self.mpg))}')
        return hash((self.make,self.model,self.year,self.mpg))

class AutoMPGData:
    ''' Class that contains methods to parse a data file containing information
    on automobiles, and creates a list of AutoMPG types from that file.
    '''

    def __init__(self):
        logger.debug('creating AugoMPGData object')
        self.data = list()
        self._load_data()

    def __iter__(self):
        ## take advantage of the built in iterator for list types
        logger.debug('creating iterator for AutoMPGData')
        return iter(self.data)

    def _load_data(self):
        ''' Loads data from a 'cleaned' file; if the cleaned file doesn't exist,
        calls the method that creates a cleaned file and then proceeds. Reads
        each line of the data file, parses out the fields of interest, and creates
        AutoMPG types with that data.
        '''
        ## define a function to clean make data
        def replace_all(text):
            rep = [['chevroelt','chevrolet'],
                ['chevy','chevrolet'],
                ['maxda','mazda'],
                ['mercedes-benz','mercedes'],
                ['toyouta','toyota'],
                ['vokswagen','volkswagen'],
                ['vw','volkswagen']]
            for i in rep:
                text = text.replace(i[0], i[1])
            return text

        Record = namedtuple('Record',['mpg','cylinders','displacement',\
                            'horsepower','weight','acceleration','modelYear',\
                            'origin','carName'])

        if not os.path.exists("auto-mpg.clean.txt"):
            self._clean_data()

        with open("auto-mpg.clean.txt") as csvfile:
            logger.debug('reading cleaned data')
            mpgreader = csv.reader(csvfile,delimiter=' ',skipinitialspace=True)

            for row in mpgreader:
                rec = Record(*row)
                logger.debug(f'parsing {rec}')
                ## The first word of the carname is the make, all following words
                ## are the model
                make = rec.carName.split()[0]
                ## Clean make data
                make = replace_all(make)
                model = ' '.join(word for word in rec.carName.split()[1:])
                self.data.append(AutoMPG(make,model,'19'+rec.modelYear,rec.mpg))


    def _clean_data(self):
        ''' Called only when a 'cleaned' data file doesn't exist. A cleaned file
        means one in which all tabs have been converted to spaces to make parsing
        the file easier.
        '''
        logger.info('\"auto-mpg.clean.txt\" does not exist, cleaning \"auto-mpg.data.txt\"')

        if not os.path.exists("auto-mpg.data.txt"):
            self._get_data()

        with open("auto-mpg.data.txt") as inFile:
            with open("auto-mpg.clean.txt","w") as outFile:
                for line in inFile:
                    outFile.write(line.expandtabs(1))

    def _get_data(self):
        ''' Called only when neither autompg.clean.txt nor autompg.data.txt exist.
        Downloads autompg.data.txt from https://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data
        '''
        logger.info('\"auto-mpg.data.txt\" does not exist, downloading from https://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data')

        try:
            response = requests.get('https://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data')
            if response:
                with open ("auto-mpg.data.txt", "wb") as f:
                    for line in response:
                        f.write(line)
            else:
                logger.error(f'received response {response.status_code} when downloading from https://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data')
                sys.exit(1)

        except requests.exceptions.RequestException as e:
            logger.error(e)
            sys.exit(1)

    def mpg_by_year(self):
        logger.info('creating a dict to store the average MPG in each year')
        dic = {}
        for i in self.data:
            if i.year in dic:
                dic[i.year][0] += float(i.mpg)
                dic[i.year][1] += 1
            else:
                dic[i.year] = [float(i.mpg), 1]
        for k in dic.keys():
            dic[k] = dic[k][0]/dic[k][1]
        return dic

    def mpg_by_make(self):
        logger.info('creating a dict to store the average MPG in each manufacturer')
        dic = {}
        for i in self.data:
            if i.make in dic:
                dic[i.make][0] += float(i.mpg)
                dic[i.make][1] += 1
            else:
                dic[i.make] = [float(i.mpg), 1]
        for k in dic.keys():
            dic[k] = dic[k][0]/dic[k][1]
        return dic

    def sort_by_default(self):
        logger.info('sorting data by default')
        list.sort(self.data)

    def sort_by_year(self):
        logger.info('sorting data by year, make, model, mpg')
        list.sort(self.data)
        list.sort(self.data, key = (lambda x: int(x.year)))

    def sort_by_mpg(self):
        logger.info('sorting data by mpg, make, model, year')
        list.sort(self.data)
        list.sort(self.data, key = (lambda x: float(x.mpg)))



def createLogger():
    global logger

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    fh = logging.FileHandler('autompg3.log', 'w')
    fh.setLevel(logging.DEBUG)

    logger.addHandler(fh)

    sh = logging.StreamHandler()
    sh.setLevel(logging.INFO)
    logger.addHandler(sh)

def main():
    parser = argparse.ArgumentParser(description = 'analyze Auto MPG data set')

    parser.add_argument('command', metavar='<command>', choices=['print'], help='command to execute')
    parser.add_argument('-s', '--sort', metavar='<sort order>', choices=['year','mpg','default'], action='store', dest='sort_order')
    parser.add_argument('-o', '--ofile', metavar='<outfile>', type=str, help='output file name(should end with .csv)')
    parser.add_argument('-p', '--plot', action='store_true', help='turn on matplotlib')
    parser.add_argument('-y', '--mpgyear', action='store_true', help='turn on mpg_by_year method and print it into csv file')
    parser.add_argument('-m', '--mpgmake', action='store_true', help='turn on mpg_by_make method and print it into csv file')

    args = parser.parse_args()
    logger.info(f'received arguments from command line: {args}')

    print(args)

    data = AutoMPGData()

    if args.sort_order:
        if args.sort_order == 'default':
            data.sort_by_default()
        elif args.sort_order == 'year':
            data.sort_by_year()
        elif args.sort_order == 'mpg':
            data.sort_by_mpg()
        else:
            logger.error(f'invalid sort order: {args.sort_order}')

    if args.plot:
        years = []
        for car in data:
            years.append(int(car.year))
        plt.hist(years)
        plt.show()

    if args.command == 'print':
        logger.debug('printing AutoMPG data in csvfile')
        if args.ofile != None:
            csvfile = open(args.ofile, "w", encoding='utf8', newline="")
        else:
            csvfile = open('sys.stdout', "w", encoding='utf8', newline="")
        writer = csv.writer(csvfile)
        writer.writerow(['make', 'model', 'year', 'mpg'])
        for car in data:
            writer.writerow([car.make, car.model, car.year, car.mpg])
        if args.mpgyear:
            dic = data.mpg_by_year()
            writer.writerow(['year', 'mpg'])
            for k in dic.keys():
                writer.writerow([k, dic[k]])
        if args.mpgmake:
            dic = data.mpg_by_make()
            writer.writerow(['make', 'mpg'])
            for k in dic.keys():
                writer.writerow([k, dic[k]])
    else:
        logger.error(f'invalid command: {args.command}')

createLogger()

if __name__ == '__main__':
    main()