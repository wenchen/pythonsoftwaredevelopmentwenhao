# Wenhao Chen ID:873510615
import blackjack2 as bj
import unittest
import sys
from collections import namedtuple

Score = namedtuple('Score', 'total soft_ace_count')

class TestBlackjack(unittest.TestCase):
	def setUp(self):
		self.percentage = 0

	def test_score(self):
		self.assertEqual(bj.score([3, 12]), Score(13, 0))
		self.assertEqual(bj.score([5, 5, 10]), Score(20, 0))
		self.assertEqual(bj.score([11, 10, 1]), Score(21, 0))
		self.assertEqual(bj.score([1, 1, 5]), Score(17, 1))
		self.assertEqual(bj.score([1, 1, 1, 7]), Score(20, 1))
		self.assertEqual(bj.score([7, 8, 10]), Score(25, 0))
		self.assertEqual(bj.score([1, 6, 8, 10]), Score(25, 0))
if __name__ == '__main__':
	unittest.main()