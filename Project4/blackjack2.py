# Wenhao Chen ID:873510615
# This project is to simulate the percentage that the hand of dealer was bust.
import random
import sys
from collections import namedtuple
import csv

Score = namedtuple('Score', 'total soft_ace_count')
Stand = namedtuple('Stand', 'stand total')

def get_card():
	return random.randint(1,13)

def score(cards):
	total = 0
	soft_ace_count = 0
	for i in cards:
		# J,Q,K situation
		if i == 11 or i == 12 or i == 13:
			total = total + 10
			# soft ace can avoid bust
			if total > 21 and soft_ace_count == 1:
				total = total - 10
				soft_ace_count = 0
		# Ace situation
		elif i == 1:
			if total + 11 <= 21:
				total = total + 11
				soft_ace_count = 1
			else:
				total = total + 1
			# soft ace can avoid bust
			if total > 21 and soft_ace_count == 1:
				total = total - 10
				soft_ace_count = 0
		# 2-10 situation
		else:
			total = total + i
			# soft ace can avoid bust
			if total > 21 and soft_ace_count == 1:
				total = total - 10
				soft_ace_count = 0
	return Score(total, soft_ace_count)


def stand(stand_on_value, stand_on_soft, cards): 
	total, soft_ace_count = score(cards)
	if stand_on_soft == True:
		if total < stand_on_value:
			return Stand(False, total)
		else:
			return Stand(True, total)
	if stand_on_soft == False:
		if total < stand_on_value:
			return Stand(False, total)
		elif total == stand_on_value and soft_ace_count == 1:
			return Stand(False, total)
		else:
			return Stand(True, total)

def play_hand(stand_on_value, stand_on_soft):
	cards = [get_card()]
	cards.append(get_card())
	while stand(stand_on_value, stand_on_soft, cards).stand == False:
		cards.append(get_card())
	total = stand(stand_on_value, stand_on_soft, cards).total
	if total>21:
		return 22
	else:
		return total

def main():
	arg = sys.argv
	# perform basic validation on the arguments
	arg[1] = int(arg[1])
	if arg[1]<=0:
		raise ValueError('The arguments are illegal')
	# simulations
	defaultdict = {}
	for stdv in range(13,21):
		defaultdict[f'H{stdv}'] = [0 for k in range(10)]
		defaultdict[f'S{stdv}'] = [0 for k in range(10)]
		for i in range(arg[1]):
			totalH = play_hand(stdv, False)
			defaultdict[f'H{stdv}'][totalH-13] += 1
			totalS = play_hand(stdv, True)
			defaultdict[f'S{stdv}'][totalS-13] += 1
	# write into CSV
	with open('blackjack.csv', "w", encoding='utf8', newline="") as csvfile:
		writer = csv.writer(csvfile)
		writer.writerow(['Strategy', '13', '14', '15', '16', '17', '18', '19', '20', '21', 'Bust'])
		for key in defaultdict.keys():
			writer.writerow([key] + defaultdict[key])

if __name__ == '__main__':
	print(main())