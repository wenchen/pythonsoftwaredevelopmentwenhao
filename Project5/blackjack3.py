# Wenhao Chen ID:873510615
# This project is to simulate the percentage that the hand of dealer and player.
import random
import sys
import csv

class Hand():
	def __init__(self):
		self.cards = []
		self.total = 0
		self.soft_ace_count = 0
	def __str__(self):
		return str(self.cards)
	def add_card(self):
		self.cards.append(random.randint(1,13))
		self.score()
	def is_blackjack(self):
		return self.total == 21 and len(self.cards) == 2
	def is_bust(self):
		return self.total > 21
	# The reason why I don't use loop is that the the previous total score have been
	# stored in the "total" attribute. Thus, we only need to calculate total score by
	# adding the score of new card (the last card). This way is more efficient because
	# we don't need to recalculate all the thing every time! Efficiency is very important
	# for data scientist!
	def score(self):
		new_card = self.cards[-1]
		# J,Q,K situation
		if new_card == 11 or new_card == 12 or new_card == 13:
			self.total += 10
			# soft ace can avoid bust
			if self.total > 21 and self.soft_ace_count == 1:
				self.total -= 10
				self.soft_ace_count = 0
		# Ace situation
		elif new_card == 1:
			if self.total + 11 <= 21:
				self.total += 11
				self.soft_ace_count = 1
			else:
				self.total += 1
			# soft ace can avoid bust
			if self.total > 21 and self.soft_ace_count == 1:
				self.total -= 10
				self.soft_ace_count = 0
		# 2-10 situation
		else:
			self.total += new_card
			# soft ace can avoid bust
			if self.total > 21 and self.soft_ace_count == 1:
				self.total -= 10
				self.soft_ace_count = 0

class Strategy():
	def __init__(self, stand_on_value, stand_on_soft):
		self.stand_on_value = stand_on_value
		self.stand_on_soft = stand_on_soft
	def __repr__(self):
		return 'canonical'
	def __str__(self):
		if self.stand_on_soft:
			return f'S{self.stand_on_value}'
		else:
			return f'H{self.stand_on_value}'
	# Determine whether stand
	def stand(self, total, soft_ace_count):
		if self.stand_on_soft:
			if total < self.stand_on_value:
				return False
			else:
				return True
		else:
			if total < self.stand_on_value:
				return False
			elif total == self.stand_on_value and soft_ace_count == 1:
				return False
			else:
				return True
	# Simulate a hand and return the hand object
	def play(self):
		hand = Hand()
		while not self.stand(hand.total, hand.soft_ace_count):
			hand.add_card()
		return hand

def main():
	arg = sys.argv
	# perform basic validation on the arguments
	arg[1] = int(arg[1])
	if arg[1]<=0:
		raise ValueError('The arguments are illegal')
	# Simulation
	defaultdict = {}
	# loop through all the player situations
	for stdvp in range(13,21):
		for sftp in [False, True]:
			player = Strategy(stdvp, sftp)
			defaultdict[f'P-{str(player)}'] = []
			# loop through all the dealer situations
			for stdvd in range(13,21):
				for sftd in [False, True]:
					dealer = Strategy(stdvd, sftd)
					# loop throuth the simulation in each situation
					# count the the number of win and tie to calculate the win rate
					win = 0
					tie = 0
					for i in range(arg[1]):
						Phand = player.play()
						# if player is bust, the player lost this time
						if Phand.is_bust():
							continue
						else:
							Dhand = dealer.play()
							# if dealer is bust, the player win this time
							if Dhand.is_bust():
								win += 1
							else:
								# if player's total score is bigger, the player win this time
								if Phand.total > Dhand.total:
									win += 1
								# Consider whether or not tie
								# if either of player's hand or dealer's hand is blackjack, the blackjack one win
								# else tie
								elif Phand .total == Dhand.total:
									if Phand.is_blackjack() and not Dhand.is_blackjack():
										win += 1
									elif not Phand.is_blackjack() and not Dhand.is_blackjack():
										tie +=1
									elif Phand.is_blackjack() and Dhand.is_blackjack():
										tie +=1
					defaultdict[f'P-{str(player)}'].append(f'{round(win/(arg[1]-tie)*100,2)}%')
	# write into CSV
	with open('blackjack3.csv', "w", encoding='utf8', newline="") as csvfile:
		writer = csv.writer(csvfile)
		writer.writerow(['P-Strategy', 'D-H13', 'D-S13', 'D-H14', 'D-S14', 'D-H15', 'D-S15', 'D-H16', 'D-S16', 'D-H17', 'D-S17', 'D-H18', 'D-S18', 'D-H19', 'D-S19', 'D-H20', 'D-S20'])
		for key in defaultdict.keys():
			writer.writerow([key] + defaultdict[key])

if __name__ == '__main__':
	print(main())