# Wenhao Chen ID:873510615
import blackjack3 as bj
import unittest
import sys

class TestBlackjack(unittest.TestCase):
	def setUp(self):
		self.hand1 = bj.Hand()
		for i in [3, 12]:
			self.hand1.cards.append(i)
			self.hand1.score()
		self.hand2 = bj.Hand()
		for i in [11, 10, 1]:
			self.hand2.cards.append(i)
			self.hand2.score()
		self.hand3 = bj.Hand()
		for i in [1, 12]:
			self.hand3.cards.append(i)
			self.hand3.score()
		self.hand4 = bj.Hand()
		for i in [1, 6, 8, 10]:
			self.hand4.cards.append(i)
			self.hand4.score()
		self.strategy1 = bj.Strategy(17, True)
		self.strategy2 = bj.Strategy(17, False)

	def test_score(self):
		self.assertEqual(self.hand1.total, 13)
		self.assertEqual(self.hand2.total, 21)
		self.assertEqual(self.hand3.total, 21)
		self.assertEqual(self.hand4.total, 25)

		self.assertEqual(self.hand1.soft_ace_count, 0)
		self.assertEqual(self.hand2.soft_ace_count, 0)
		self.assertEqual(self.hand3.soft_ace_count, 1)
		self.assertEqual(self.hand4.soft_ace_count, 0)

	def test_bust(self):
		self.assertEqual(self.hand1.is_bust(), False)
		self.assertEqual(self.hand2.is_bust(), False)
		self.assertEqual(self.hand3.is_bust(), False)
		self.assertEqual(self.hand4.is_bust(), True)

	def test_blackjack(self):
		self.assertEqual(self.hand1.is_blackjack(), False)
		self.assertEqual(self.hand2.is_blackjack(), False)
		self.assertEqual(self.hand3.is_blackjack(), True)
		self.assertEqual(self.hand4.is_blackjack(), False)

	def test_stand(self):
		self.assertEqual(self.strategy1.stand(17, 1), True)
		self.assertEqual(self.strategy1.stand(17, 0), True)
		self.assertEqual(self.strategy2.stand(17, 1), False)
		self.assertEqual(self.strategy2.stand(17, 0), True)


if __name__ == '__main__':
	unittest.main()