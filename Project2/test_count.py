# Wenhao Chen ID:873510615
# the default characters are 'Abracadabra!'
import count as c
import unittest
import sys

all_letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
all_letters_lower = 'abcdefghijklmnopqrstuvwxyz'
path = r"F:\CourseWork\Pydev\characters.txt"

class TestCountMethods(unittest.TestCase):

	def setUp(self):
		self.d = {'a':5, 'b':2, 'c':1, 'd':1, 'r':2}
		self.dc = {'a':4, 'b':2, 'c':1, 'd':1, 'r':2, 'A':1}
		self.dl = {'a':5, 'b':2, 'c':1}
		self.dcl = {'a':4, 'b':2, 'c':1, 'A':1}

		self.dz = {}
		for i in all_letters_lower:
			self.dz[i] = 0
		self.dz['a'] = 5
		self.dz['b'] = 2
		self.dz['c'] = 1
		self.dz['d'] = 1
		self.dz['r'] = 2

		self.dcz = {}
		for i in all_letters:
			self.dcz[i] = 0
		self.dcz['a'] = 4
		self.dcz['b'] = 2
		self.dcz['c'] = 1
		self.dcz['d'] = 1
		self.dcz['r'] = 2
		self.dcz['A'] = 1

		self.dlz = {'a':5, 'b':2, 'c':1, 'z':0}
		self.dclz = {'a':4, 'b':2, 'c':1, 'z':0, 'A':1}

	def test_null(self):
		sys.argv = ['count.py', path]
		self.assertEqual(c.main(), self.d)

	def test_c(self):
		sys.argv = ['count.py', '-c', path]
		self.assertEqual(c.main(), self.dc)

	def test_l(self):
		sys.argv = ['count.py', '-l', 'Aabcz', path]
		self.assertEqual(c.main(), self.dl)

	def test_z(self):
		sys.argv = ['count.py', '-z', path]
		self.assertEqual(c.main(), self.dz)

	def test_cl(self):
		sys.argv = ['count.py', '-c', '-l', 'Aabcz', path]
		self.assertEqual(c.main(), self.dcl)

	def test_cz(self):
		sys.argv = ['count.py', '-c', '-z', path]
		self.assertEqual(c.main(), self.dcz)

	def test_lz(self):
		sys.argv = ['count.py', '-l', 'Aabcz', '-z', path]
		self.assertEqual(c.main(), self.dlz)

	def test_clz(self):
		sys.argv = ['count.py', '-c', '-l', 'Aabcz', '-z', path]
		self.assertEqual(c.main(), self.dclz)

if __name__ == '__main__':
	unittest.main()