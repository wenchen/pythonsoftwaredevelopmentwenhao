# Wenhao Chen ID:873510615
import unittest
import final_project_Adult as Adult
import final_project_Bank as Bank

class TestFinal(unittest.TestCase):

	def test_init(self):
		self.assertEqual(self.adult1.age, 3)
		self.assertEqual(self.adult1.education, 5)
		self.assertEqual(self.adult1.income, '>50K')

		self.assertEqual(self.bank1.age, 3)
		self.assertEqual(self.bank1.education, 5)
		self.assertEqual(self.bank1.loan, 'yes')

	def setUp(self):
		self.adult1 = Adult.Adult(42,14,'>50K')
		self.adult2 = Adult.Adult(57,12,'<=50K')
		self.adult3 = Adult.Adult(42,14,'>50K')

		self.bank1 = Bank.Bank(42,'professional.course','yes')
		self.bank2 = Bank.Bank(57,'university.degree','no')
		self.bank3 = Bank.Bank(42,'professional.course','yes')

	def test_equal(self):
		self.assertEqual(self.adult1, self.adult3)
		self.assertNotEqual(self.adult1, self.adult2)

		self.assertEqual(self.bank1, self.bank3)
		self.assertNotEqual(self.bank1, self.bank2)

	def test_hash(self):
		self.assertEqual(hash(self.adult1), hash(self.adult3))
		self.assertNotEqual(hash(self.adult1), hash(self.adult2))

		self.assertEqual(hash(self.adult1), hash(self.adult3))
		self.assertNotEqual(hash(self.adult1), hash(self.adult2))

	def test_classmethod(self):
		self.assertEqual(Adult.Adult.num_instances, 3)
		self.assertEqual(Bank.Bank.num_instances, 3)

if __name__ == '__main__':
	unittest.main()