# Wenhao Chen ID:873510615
# This part is to print the Bank data set in a csv file and display 4 plots we need(by using argparse)
import os
import csv
import logging
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import requests
import zipfile

class Bank():
	# to represent person with information of age level, eudcation level and loan(yes or no)
	num_instances = 0
	def __init__(self, age, education, loan):
		logger.debug(f'creating Bank object: age = {age}, education = {education}, loan = {loan}')
		if age < 30:
			self.age = 1
		elif age < 40:
			self.age = 2
		elif age < 50:
			self.age = 3
		elif age <60:
			self.age = 4
		else:
			self.age = 5

		if education == 'basic.4y' or education == 'basic.6y' or education == 'illiterate':
			self.education = 1
		elif education == 'basic.9y':
			self.education = 2
		elif education == 'high.school':
			self.education = 3
		elif education == 'university.degree':
			self.education = 4
		elif education == 'professional.course':
			self.education = 5

		self.loan = loan
		Bank.num_instances += 1
		
	def __repr__(self):
		return f"{self.age}, {self.education}, {self.loan}"

	def __eq__(self,other):
		if type(self) == type(other):
			logger.debug(f'comparing {self.__str__()} == {other.__str__()}: {self.age == other.age and self.education == other.education and self.loan == other.loan}')
			return self.age == other.age and self.education == other.education and self.loan == other.loan
		else:
			logger.error(f'equality not implemented in Bank class for types {type(self)} and {type(other)}')
			return NotImplemented

	def __hash__(self):
		# Since all instance variables are immutable types, put them in a tuple
		# and hash the tuple
		return hash((self.age,self.education,self.loan))

	@classmethod
	def get_instance(cls):
		return cls.num_instances

class BankData():
	# to get information we need from the data set and make is as a list
	def __init__(self):
		logger.debug('creating BankData object')
		self.data = []
		self.load_data()

	def __iter__(self):
		logger.debug('creating iterator for BankData')
		return iter(self.data)

	def load_data(self):
		# get the information we need
		if not os.path.exists("bank-additional/bank-additional-full.csv"):
			self.get_data()

		with open("bank-additional/bank-additional-full.csv") as csvfile:
			logger.debug('reading Bank data')
			reader = csv.reader(csvfile,delimiter=';',skipinitialspace=True)
			for row in reader:
				if row[3] != 'unknown' and row[6] != 'unknown' and row[3] != 'education':
					self.data.append(Bank(int(row[0]), row[3], row[6]))

	def get_data(self):
		# download the data set
		logger.info('download data')
		try:
			response = requests.get('https://archive.ics.uci.edu/ml/machine-learning-databases/00222/bank-additional.zip')
			if response:
				with open ("bank-additional.zip", "wb") as f:
					f.write(response.content)
				with zipfile.ZipFile('bank-additional.zip', 'r') as z:
					z.extract('bank-additional/bank-additional-full.csv')
			else:
				logger.error(f'received response {response.status_code} when downloading the data')
				sys.exit(1)

		except requests.exceptions.RequestException as e:
			ogger.error(e)
			sys.exit(1)

	def manipulate_data(self):
		# calculate the number of people in each level of age and education
		logger.debug('calculate the number of people in each level of age and education')
		age = [[0, 0] for i in range(5)]
		edu = [[0, 0] for i in range(5)]
		for obj in self.data:
			age[obj.age - 1][0] += 1
			edu[obj.education - 1][0] += 1
			if obj.loan == 'yes':
				age[obj.age - 1][1] += 1
				edu[obj.education - 1][1] += 1
		return age, edu

def createLogger():
	global logger

	logger = logging.getLogger()
	logger.setLevel(logging.DEBUG)

	fh = logging.FileHandler('Bank.log', 'w')
	fh.setLevel(logging.DEBUG)

	logger.addHandler(fh)

	sh = logging.StreamHandler()
	sh.setLevel(logging.INFO)
	logger.addHandler(sh)

def main():
	parser = argparse.ArgumentParser(description = 'analyze Bank data set')

	parser.add_argument('command', metavar='<command>', choices=['print'], help='command to execute')
	parser.add_argument('-p', '--plot', action='store_true', help='show 4 plots')
	parser.add_argument('-o', '--ofile', metavar='<outfile>', type=str, help='output file name(should end with .csv)')

	args = parser.parse_args()
	logger.info(f'received arguments from command line: {args}')
	print(args)

	data = BankData()

	if args.plot:
		logger.info('show 4 plots of Bank data set')
		age, edu = data.manipulate_data()
		age_pop = []
		age_loan = []
		edu_pop = []
		edu_loan = []
		for i in age:
			age_pop.append(i[0])
			age_loan.append(i[1])
		for i in edu:
			edu_pop.append(i[0])
			edu_loan.append(i[1])

		plt.subplot(2, 2, 1)
		plt.bar([1,2,3,4,5], age_pop)
		plt.title("distribution(age)")
		plt.xlabel("age level")
		plt.ylabel("number of people")

		plt.subplot(2, 2, 2)
		plt.bar([1,2,3,4,5], edu_pop)
		plt.title("distribution (education)")
		plt.xlabel("education level")
		plt.ylabel("number of people")

		plt.subplot(2, 2, 3)
		plt.bar([1,2,3,4,5], list(np.array(age_loan)/np.array(age_pop)))
		plt.title("proportion of having loan (age)")
		plt.xlabel("age level")
		plt.ylabel("proportion")

		plt.subplot(2, 2, 4)
		plt.bar([1,2,3,4,5], list(np.array(edu_loan)/np.array(edu_pop)))
		plt.title("proportion of having loan (education)")
		plt.xlabel("education level")
		plt.ylabel("proportion")

		plt.show()

	if args.command == 'print':
		logger.debug('printing cleaned Bank data in csvfile')
		if args.ofile != None:
			csvfile = open(args.ofile, "w", encoding='utf8', newline="")
		else:
			csvfile = open('sys.stdout', "w", encoding='utf8', newline="")
		writer = csv.writer(csvfile)
		writer.writerow(['age_level', 'education_level', 'loan'])
		for i in data:
			writer.writerow([i.age, i.education, i.loan])

	else:
		logger.error(f'invalid command: {args.command}')

createLogger()

if __name__ == '__main__':
	main()