# Wenhao Chen ID:873510615
# PS: all parts of the project is produced individually
# This part is to combine those two data sets to find the relation between income level and loan.
import final_project_Adult as Adult
import final_project_Bank as Bank
import matplotlib.pyplot as plt
import numpy as np

def main():
	data_Adult = Adult.AdultData()
	data_Bank = Bank.BankData()

	ageA, eduA = data_Adult.manipulate_data()
	ageB, eduB = data_Bank.manipulate_data()

	ageA_pop = []
	ageA_income = []
	eduA_pop = []
	eduA_income = []

	ageB_pop = []
	ageB_loan = []
	eduB_pop = []
	eduB_loan = []

	for i in ageA:
		ageA_pop.append(i[0])
		ageA_income.append(i[1])
	for i in eduA:
		eduA_pop.append(i[0])
		eduA_income.append(i[1])

	for i in ageB:
		ageB_pop.append(i[0])
		ageB_loan.append(i[1])
	for i in eduB:
		eduB_pop.append(i[0])
		eduB_loan.append(i[1])

	plt.subplot(2, 2, 1)
	plt.bar([1,2,3,4,5], list(np.array(ageA_income)/np.array(ageA_pop)))
	plt.title("proportion of having income (age)")
	plt.xlabel("age level")
	plt.ylabel("proportion")

	plt.subplot(2, 2, 2)
	plt.bar([1,2,3,4,5,6], list(np.array(eduA_income)/np.array(eduA_pop)))
	plt.title("proportion of having income (education)")
	plt.xlabel("education level")
	plt.ylabel("proportion")

	plt.subplot(2, 2, 3)
	plt.bar([1,2,3,4,5], list(np.array(ageB_loan)/np.array(ageB_pop)))
	plt.title("proportion of having loan (age)")
	plt.xlabel("age level")
	plt.ylabel("proportion")

	plt.subplot(2, 2, 4)
	plt.bar([1,2,3,4,5], list(np.array(eduB_loan)/np.array(eduB_pop)))
	plt.title("proportion of having loan (education)")
	plt.xlabel("education level")
	plt.ylabel("proportion")

	plt.show()

if __name__ == '__main__':
	main()

'''
Analysis: 
From the plots of proportion, we can see there is obvious relation between age & education level
and income level(shown as the proportion of people whose income > 50K). This is in line with our
common sense that people who get higher level education may gain higher income, and in most cases,
the income level is increasing with our age (age<60).

In the other hand, there are always about 15% people have loan from bank, no matter what age or
educaton level they are in.

Conclusion:
There is no significant relation between income and personal loan from the information of those
two data sets.
'''