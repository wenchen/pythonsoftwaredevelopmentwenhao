# Wenhao Chen ID:873510615
# This part is to print the Adult data set in a csv file and display 4 plots we need(by using argparse)
import os
import csv
import logging
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import requests

class Adult():
	# to represent person with information of age level, eudcation level and income level(>50K or <=50K)
	num_instances = 0
	def __init__(self, age, education, income):
		logger.debug(f'creating Adult object: age = {age}, education = {education}, income = {income}')
		if age < 30:
			self.age = 1
		elif age < 40:
			self.age = 2
		elif age < 50:
			self.age = 3
		elif age <60:
			self.age = 4
		else:
			self.age = 5

		if education <= 6:
			self.education = 1
		elif education <= 9:
			self.education = 2
		elif education <=12:
			self.education = 3
		elif education == 13:
			self.education = 4
		elif education <= 15:
			self.education = 5
		else:
			self.education = 6

		self.income = income
		Adult.num_instances += 1
		
	def __repr__(self):
		return f"{self.age}, {self.education}, {self.income}"

	def __eq__(self,other):
		if type(self) == type(other):
			logger.debug(f'comparing {self.__str__()} == {other.__str__()}: {self.age == other.age and self.education == other.education and self.income == other.income}')
			return self.age == other.age and self.education == other.education and self.income == other.income
		else:
			logger.error(f'equality not implemented in Adult class for types {type(self)} and {type(other)}')
			return NotImplemented

	def __hash__(self):
		# Since all instance variables are immutable types, put them in a tuple
		# and hash the tuple
		return hash((self.age,self.education,self.income))

	@classmethod
	def get_instance(cls):
		return cls.num_instances

class AdultData():
	# to get information we need from the data set and make is as a list
	def __init__(self):
		logger.debug('creating AdultData object')
		self.data = []
		self.load_data()

	def __iter__(self):
		logger.debug('creating iterator for AdultData')
		return iter(self.data)

	def load_data(self):
		# get the information we need
		if not os.path.exists("adult.data"):
			self.get_data()

		with open("adult.data") as csvfile:
			logger.debug('reading adult data')
			reader = csv.reader(csvfile,delimiter=',',skipinitialspace=True)
			for row in reader:
				if len(row) == 15:
					self.data.append(Adult(int(row[0]), int(row[4]), row[-1]))

	def get_data(self):
		# download the data set
		logger.info('download data')
		try:
			response = requests.get('https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data')
			if response:
				with open ("adult.data", "wb") as f:
					for line in response:
						f.write(line)
			else:
				logger.error(f'received response {response.status_code} when downloading the data')
				sys.exit(1)

		except requests.exceptions.RequestException as e:
			ogger.error(e)
			sys.exit(1)

	def manipulate_data(self):
		# calculate the number of people in each level of age and education
		logger.debug('calculate the number of people in each level of age and education')
		age = [[0, 0] for i in range(5)]
		edu = [[0, 0] for i in range(6)]
		for obj in self.data:
			age[obj.age - 1][0] += 1
			edu[obj.education - 1][0] += 1
			if obj.income == '>50K':
				age[obj.age - 1][1] += 1
				edu[obj.education - 1][1] += 1
		return age, edu

def createLogger():
	global logger

	logger = logging.getLogger()
	logger.setLevel(logging.DEBUG)

	fh = logging.FileHandler('Adult.log', 'w')
	fh.setLevel(logging.DEBUG)

	logger.addHandler(fh)

	sh = logging.StreamHandler()
	sh.setLevel(logging.INFO)
	logger.addHandler(sh)

def main():
	parser = argparse.ArgumentParser(description = 'analyze Adult data set')

	parser.add_argument('command', metavar='<command>', choices=['print'], help='command to execute')
	parser.add_argument('-p', '--plot', action='store_true', help='show 4 plots')
	parser.add_argument('-o', '--ofile', metavar='<outfile>', type=str, help='output file name(should end with .csv)')

	args = parser.parse_args()
	logger.info(f'received arguments from command line: {args}')
	print(args)

	data = AdultData()

	if args.plot:
		logger.info('show 4 plots of Adult data set')
		age, edu = data.manipulate_data()
		age_pop = []
		age_income = []
		edu_pop = []
		edu_income = []
		for i in age:
			age_pop.append(i[0])
			age_income.append(i[1])
		for i in edu:
			edu_pop.append(i[0])
			edu_income.append(i[1])

		plt.subplot(2, 2, 1)
		plt.bar([1,2,3,4,5], age_pop)
		plt.title("distribution(age)")
		plt.xlabel("age level")
		plt.ylabel("number of people")

		plt.subplot(2, 2, 2)
		plt.bar([1,2,3,4,5,6], edu_pop)
		plt.title("distribution (education)")
		plt.xlabel("education level")
		plt.ylabel("number of people")

		plt.subplot(2, 2, 3)
		plt.bar([1,2,3,4,5], list(np.array(age_income)/np.array(age_pop)))
		plt.title("proportion of income >50k (age)")
		plt.xlabel("age level")
		plt.ylabel("proportion")

		plt.subplot(2, 2, 4)
		plt.bar([1,2,3,4,5,6], list(np.array(edu_income)/np.array(edu_pop)))
		plt.title("proportion of income >50k (education)")
		plt.xlabel("education level")
		plt.ylabel("proportion")

		plt.show()

	if args.command == 'print':
		logger.debug('printing cleaned Adult data in csvfile')
		if args.ofile != None:
			csvfile = open(args.ofile, "w", encoding='utf8', newline="")
		else:
			csvfile = open('sys.stdout', "w", encoding='utf8', newline="")
		writer = csv.writer(csvfile)
		writer.writerow(['age_level', 'education_level', 'income'])
		for i in data:
			writer.writerow([i.age, i.education, i.income])

	else:
		logger.error(f'invalid command: {args.command}')

createLogger()

if __name__ == '__main__':
	main()