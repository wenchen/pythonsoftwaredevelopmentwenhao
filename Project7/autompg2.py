# Wenhao Chen ID:873510615
# This assignment is to display the auto mpg dataset and sort is by using argparse
import os
from collections import namedtuple
import logging
import requests
import argparse

logging.basicConfig(filename='autompg2.log', level=logging.DEBUG, filemode='w')
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

Record = namedtuple('Record', ['mpg', 'cylinders', 'displacement', 'horsepower', 'weight', 'acceleration', 'model_year', 'origin', 'car_name'])

class AutoMPG():
	def __init__(self, make, model, year, mpg):
		self.make = make
		self.model = model
		self.year = year
		self.mpg = mpg
	
	def __repr__(self):
		return f'{self.make}, {self.model}, {self.year}, {self.mpg}'
	
	def __eq__(self, other):
		if type(self) == type(other):
			return self.make == other.make and self.model == other.model and self.year == other.year and self.mpg == other.mpg
		else:
			return NotImplemented
			
	def __lt__(self,other):
		''' Compares the make lexicographically: if they are equal, compares the model;
		if they are equal, compares the year; if they are equal, compares the mpg
		'''

		if type(self) == type(other):
			if self.make != other.make:
				return self.make < other.make
			if self.model != other.model:
				return self.model < other.model
			if self.year != other.year:
				return self.year < other.year

			return self.mpg < other.mpg

		else:
			return NotImplemented

	def __hash__(self):
		return hash((self.make, self.model, self.year, self.mpg))

class AutoMPGData():
	def __init__(self):
		self.data = self._load_data()
		self.index = 0

	def __iter__(self):
		return self

	def __next__(self):		# iterate the data
		if self.index >= len(self.data):
			raise StopIteration
		else:
			rep = self.index
			self.index += 1
			return self.data[rep]

	def sort_by_default(self):
		self.data.sort(key = lambda x: (x.car_name, int(x.model_year), float(x.mpg)))
		logging.debug('sort by default')

	def sort_by_year(self):
		self.data.sort(key = lambda x: (int(x.model_year), x.car_name, float(x.mpg)))
		logging.debug('sort by year')

	def sort_by_mpg(self):
		self.data.sort(key = lambda x: (float(x.mpg), x.car_name, int(x.model_year)))
		logging.debug('sort by mpg')

	def _get_data(self):
		# download auto mpg data and rename it
		url = "https://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data"
		r = requests.get(url) 
		with open("auto-mpg.data",'wb') as f:
			f.write(r.content)
		os.rename('auto-mpg.data', 'auto-mpg.data.txt')
		logging.debug('downloading data')
		
	def _load_data(self):
		# chack the whether path exists
		if not os.path.exists('auto-mpg.data.txt'):
			self._get_data()
		if not os.path.exists('auto-mpg.clean.txt'):
			self._clean_data()
		#load data
		data = []
		with open('auto-mpg.clean.txt') as file:
			lines = file.readlines()
			for line in lines:
				# split all the space and \n
				line = line.rstrip("\n").split()
				line[8] = ' '.join(line[8:]).rstrip('"').lstrip('"')
				line[6] = '19' + line[6]
				# put all the arguments in a namedtuple
				record = Record(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8])
				data.append(record)
		logging.debug('loading data')
		return data

	def _clean_data(self):
		# convert the TAB character to spaces and create the new file
		cleaned = []
		with open('auto-mpg.data.txt') as file:
			lines = file.readlines()
			for line in lines:
				line=line.expandtabs()
				cleaned.append(line)
		with open('auto-mpg.clean.txt', 'w') as file:
			for i in cleaned:
				file.writelines(i)
		logging.debug('cleaning data')

def main():
	# setting the parser
	parser = argparse.ArgumentParser(description='analyze Auto MPG data set')
	parser.add_argument('command', metavar='<command>', type=str, help='The only command is "print"')
	parser.add_argument("-s <sort order>", "--sort", type=str, help = 'The options to the “sort order” are “year”, “mpg”, and “default”')
	logging.debug('setting parser')

	args = parser.parse_args()
	data = AutoMPGData()
	if args.command == 'print':
		if args.sort == 'year':
			data.sort_by_year()
		elif args.sort == 'mpg':
			data.sort_by_mpg()
		else:
			data.sort_by_default()

		for a in data:
			MPG = AutoMPG(a.car_name.split()[0], ' '.join(a.car_name.split()[1:]), int(a.model_year), float(a.mpg))
			print(f'AutoMPG({MPG})')
	logging.info('everything is ok')

if __name__ == '__main__':
	print(main())