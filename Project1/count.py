# Wenhao Chen ID:873510615
# This is a Counting Characters program
import sys
import re
import csv

def add_frequencies(d, file, remove_case):
	# read the file
	f = open(file, 'r')
	characters = f.read()
	# clean up punctuations
	punc = '[,.\'"!@#$%^&*~?/<>:| ]'
	characters = re.sub(punc, '', characters)
	# add the frequency counts of the characters in the file file to the dictionary d
	for i in range(len(characters)):
		if remove_case == False:
			if characters[i] not in d.keys():
				d[characters[i]] = 1
			else:
				d[characters[i]] += 1
		elif remove_case == True:
			if characters[i].lower() not in d.keys():
				d[characters[i].lower()] = 1
			else:
				d[characters[i].lower()] += 1
	return d

def main():
	d = {}
	args = sys.argv
	all_letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
	all_letters_lower = 'abcdefghijklmnopqrstuvwxyz'
	# get the file and [-l letters] from arguments
	file = None
	for a in args:
		if a.endswith(".txt"):
			file = a
	if file == None:
		print('Please add a file in the arguments.')
		return
	# Parse the command line arguments -c, -l, -z
	else:
		if '-c' in args and '-l' not in args and '-z' not in args:
			d = add_frequencies(d, file, False)
		elif '-c' not in args and '-l' in args and '-z' not in args:
			# find the -l letters
			for i in range(len(args)):
				if args[i] == '-l':
					letter = args[i+1].lower()
			d = add_frequencies(d, file, True)
			# delete other letters
			d_temp = d.copy()
			for i in d_temp.keys():
				if i not in letter:
					del d[i]
		elif '-c' not in args and '-l' not in args and '-z' in args:
			d = add_frequencies(d, file, True)
			# add all altters into dict
			for i in all_letters_lower:
				if i not in d.keys():
					d[i] = 0
		elif '-c' in args and '-l' in args and '-z' not in args:
			# find the -l letters
			for i in range(len(args)):
				if args[i] == '-l':
					letter = args[i+1]
			d = add_frequencies(d, file, False)
			# delete other letters
			d_temp = d.copy()
			for i in d_temp.keys():
				if i not in letter:
					del d[i]
		elif '-c' in args and '-l' not in args and '-z' in args:
			d = add_frequencies(d, file, False)
			# add all altters into dict
			for i in all_letters:
				if i not in d.keys():
					d[i] = 0
		elif '-c' not in args and '-l' in args and '-z' in args:
			d = add_frequencies(d, file, True)
			# add all altters into dict
			for i in all_letters_lower:
				if i not in d.keys():
					d[i] = 0
			# find the -l letters
			for i in range(len(args)):
				if args[i] == '-l':
					letter = args[i+1].lower()
			# delete other letters
			d_temp = d.copy()
			for i in d_temp.keys():
				if i not in letter:
					del d[i]
		elif '-c' in args and '-l' in args and '-z' in args:
			d = add_frequencies(d, file, False)
			# add all altters into dict
			for i in all_letters:
				if i not in d.keys():
					d[i] = 0
			# find the -l letters
			for i in range(len(args)):
				if args[i] == '-l':
					letter = args[i+1]
			# delete other letters
			d_temp = d.copy()
			for i in d_temp.keys():
				if i not in letter:
					del d[i]
		elif '-c' not in args and '-l' not in args and '-z' not in args:
			d = add_frequencies(d, file, True)
	# print in CSV format
	l=list(d.items())
	with open('Counting_Characters.csv', "w", encoding='utf8', newline="") as csvfile:
		writer = csv.writer(csvfile)
		writer.writerows(l)

main()